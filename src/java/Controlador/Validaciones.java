/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controlador;

import java.sql.*;

/**
 *
 * @author Daniel Barra
 */
public class Validaciones extends Conexion {
    
    public boolean ValidarUsuario (String usuario,String password){
        PreparedStatement pst=null;
        ResultSet rs=null;
        
        try{
            String validarusuario = "select * from usuario where nombreusuario = ? and claveusuario = ?";
            pst=getConnection().prepareStatement(validarusuario); 
            pst.setString(1, usuario);
            pst.setString(2, password);
            rs = pst.executeQuery();
            
            if(rs.absolute(1)){
                return true;
            }
            
        }catch(Exception e){
            System.err.println("Error "+e);
        }finally{
            try{
                if(getConnection() != null) getConnection().close();
                if(pst != null)pst.close();
                if(rs != null)rs.close();
            }catch(Exception e){
               System.err.println("Error "+e); 
            }
        }
        return false;
    }
    public boolean registrar(String nombreusuario,String claveusuario,String nombre,String apellido){
       
        PreparedStatement pst=null;
        
        try{
            String validarusuario = "insert into usuario(nombreusuario,claveusuario,nombre,apellido) values (?,?,?,?)";
            pst = getConnection().prepareStatement(validarusuario);
            pst.setString(1, nombreusuario);
            pst.setString(1, claveusuario);
            pst.setString(1, nombre);
            pst.setString(1, apellido);
            
            if(pst.executeUpdate() == 1){
                return true;
            }
            
        }catch(Exception ex){
             System.err.println("Error "+ex);
        }finally{
            try{
                if(getConnection() != null) getConnection().close();
                if(pst != null)pst.close();
            }catch(Exception e){
                System.err.println("Error "+e);
            }
        }
        
        return false;
        
    }
}
  
  

 
 
 


